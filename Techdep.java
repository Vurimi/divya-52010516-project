//TechDep Class
package com.hcl.gl.assessment;

public class Techdep extends SuperDep {
	public String depName() {
		return "TECH DEPARTMENT";
	}
	public String getToadaysWork() {
		return "COMPLETE CODING OF MODULE ONE";
	}
	public String getTechStackInformation() {
		return "CORE JAVA";
	}
	public String getWorkDeadline() {
		return "COMPLETE IT BY END OF DAY";
	}

}
