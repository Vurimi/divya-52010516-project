//SuperDep class
package com.hcl.gl.assessment;

public class SuperDep {
	public String depName() {
		return "SUPERDEPARTMENT";
	}
	
	//declare method getTodaysWork of return type String
	public String getTodaysWork() {
		return "NO WORK AS OF NOW";
	}
	
	//declare method getWorkDeadline of return type String
	public String getWorkDeadline() {
		return "NIL";
	}
	
	//declare method is TodayAHoliday of type String
	public String isTodayAHoliday() {
		return "TODAY IS NOT HOLIDAY";
	}
	}
	


