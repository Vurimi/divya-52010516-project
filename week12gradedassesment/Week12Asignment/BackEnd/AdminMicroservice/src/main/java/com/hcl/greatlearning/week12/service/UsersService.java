package com.hcl.greatlearning.week12.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.greatlearning.week12.bean.Users;
import com.hcl.greatlearning.week12.dao.UsersDao;


@Service
public class UsersService {
	@Autowired
	UsersDao usersDao;
	
	public List<Users> getAllUsers() {
		return usersDao.findAll();
	}
	
	public String storeUsersInfo(Users users) {
				
						if(usersDao.existsById(users.getUid())) {
									return "user id must be unique";
						}else {
									usersDao.save(users);
									return "user details stored successfully";
						}
	}
	
	public String deleteUsersInfo(int uid) {
		if(!usersDao.existsById(uid)) {
			return "users details not present";
			}else {
			usersDao.deleteById(uid);
			return "user details deleted successfully";
			}	
	}
	
	public String updateUsersInfo(Users user) {
		if(!usersDao.existsById(user.getUid())) {
			return "Users details not present";
			}else {
			Users p	= usersDao.getById(user.getUid());	// if product not present it will give exception 
			p.setUsername(user.getUsername());						// existing product price change 
			usersDao.saveAndFlush(p);				// save and flush method to update the existing product
			return "users details updated successfully";
			}	
	}
}