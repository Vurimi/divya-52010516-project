import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Users } from '../users';
import { NgForm } from '@angular/forms';
import { MyserviceService } from '../myservice.service';
import { Books } from '../books';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  adminName:string="";
  users:Array<Users>=[];
  books:Array<Books>=[];
  storeMsg:string=""
  deleteMsg:string="";
  updateMsg:string="";
  flag:boolean = false;
  uid:number=0;
  username:string="";
  bookstoreMsg:string=""
  bookdeleteMsg:string="";
  bookupdateMsg:string="";
  flag1:boolean = false;
  id:number=0;
  name:string="";
  constructor(public router:Router,public pser:MyserviceService) { }

  ngOnInit(): void {
    let obj = sessionStorage.getItem("adminname");
    if(obj!=null){
      this.adminName=obj;

  }
}
logout() { 
  sessionStorage.removeItem("adminname");
  this.router.navigate(["login"]);
}
loadUsers(): void{
  //console.log("Event fired")
  this.pser.loadUserDetails().subscribe(res=>this.users=res);
}

storeUser(userRef:NgForm){
  //console.log(productRef.value);  
  this.pser.storeUserDetails(userRef.value).
  subscribe(res=>this.storeMsg=res,error=>console.log(error),()=>this.loadUsers());  
}

deleteUser(uid:number){
  //console.log(pid);
  this.pser.deleteUserDetails(uid).
  subscribe(res=>this.deleteMsg=res,error=>console.log(error),()=>this.loadUsers())
}

updateUser(user:Users){
  //console.log(product);
  this.flag=true;
  this.uid=user.uid;
  this.username=user.username;
}

updateUserName() {
  let user ={"uid":this.uid,"username":this.username}
  //console.log(product);
  this.pser.updateUserInfo(user).subscribe(result=>this.updateMsg=result,
  error=>console.log(error),
  ()=>{
  this.loadUsers();
  this.flag=false;  
  })
}
loadBooks(): void{
  //console.log("Event fired")
  this.pser.loadBookDetails().subscribe(res=>this.books=res);
}

storeBook(booksRef:NgForm){
  //console.log(productRef.value);  
  this.pser.storeBookDetails(booksRef.value).
  subscribe(res=>this.bookstoreMsg=res,error=>console.log(error),()=>this.loadBooks());  
}

deleteBook(id:number){
  //console.log(pid);
  this.pser.deleteBookDetails(id).
  subscribe(res=>this.bookdeleteMsg=res,error=>console.log(error),()=>this.loadBooks())
}

updateBook(books:Books){
  //console.log(product);
  this.flag1=true;
  this.id=books.id;
  this.name=books.name;
}

updateBookName() {
  let books ={"id":this.id,"name":this.name}
  //console.log(product);
  this.pser.updateBookInfo(books).subscribe(result=>this.bookupdateMsg=result,
  error=>console.log(error),
  ()=>{
  this.loadBooks();
  this.flag1=false;  
  })
}


}
