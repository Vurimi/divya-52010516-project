import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserserviceService } from '../userservice.service';

@Component({
  selector: 'app-userlogin',
  templateUrl: './userlogin.component.html',
  styleUrls: ['./userlogin.component.css']
})
export class UserloginComponent implements OnInit {
  userloginRef = new FormGroup({
    name:new FormControl(),
    password:new FormControl()
  });
  msg:string="";
  constructor(public router:Router,private UserService: UserserviceService) { }

  ngOnInit(): void {
  }
  checkUser() {
    let login = this.userloginRef.value;
    // we write this code inside a subscribe method 
    if(login.name=="Raj" && login.password=="123"){
      console.log("success");
    sessionStorage.setItem("name",login.name);
        this.router.navigate(["userHome"]);
    }else {
        this.msg = "Invalid username or password"
        console.log("failure");
    }
    this.userloginRef.reset();
  }
}
