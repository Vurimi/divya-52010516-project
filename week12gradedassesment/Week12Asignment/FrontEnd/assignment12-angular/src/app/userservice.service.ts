import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Users } from './users';
import { Books } from './books';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {
 
  constructor(public http:HttpClient) { }
  storeUserDetails(users:Users):Observable<string>{
    return this.http.post("http://localhost:8383/users/register",users,{responseType:'text'})
  } 
  loadBookDetails():Observable<Books[]> {
    return this.http.get<Books[]>("http://localhost:8383/books/getAllBooks")
  }
}

