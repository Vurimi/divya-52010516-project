import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Admin } from './admin';
import { Users } from './users';
import { Books } from './books';


@Injectable({
  providedIn: 'root'
})
export class MyserviceService {

  constructor(public http:HttpClient) { }
 // checkByCredential(admin:Admin):Observable<string>{
   // return this.http.post("http://localhost:8282/admin/login",admin,{responseType:'text'})
 // }
  storeAdminDetails(admin:Admin):Observable<string>{
    return this.http.post("http://localhost:8282/admin/register",admin,{responseType:'text'})
  } 
  loadUserDetails():Observable<Users[]> {
    return this.http.get<Users[]>("http://localhost:8282/users/getAllUsers")
  }

  storeUserDetails(user:Users):Observable<string>{
    return this.http.post("http://localhost:8282/users/storeUsers",user,{responseType:'text'})
  }  

  deleteUserDetails(uid:number):Observable<string>{
    return this.http.delete("http://localhost:8282/users/deleteUsers/"+uid,{responseType:'text'});
  }

  updateUserInfo(user:any):Observable<string>{
    return this.http.patch("http://localhost:8282/users/updateUsers",user,{responseType:'text'})
  }
  loadBookDetails():Observable<Books[]> {
    return this.http.get<Books[]>("http://localhost:8282/books/getAllBooks")
  }

  storeBookDetails(books:Books):Observable<string>{
    return this.http.post("http://localhost:8282/books/storeBooks",books,{responseType:'text'})
  }  

  deleteBookDetails(id:number):Observable<string>{
    return this.http.delete("http://localhost:8282/books/deleteBooks/"+id,{responseType:'text'});
  }

  updateBookInfo(books:any):Observable<string>{
    return this.http.patch("http://localhost:8282/books/updateBooks",books,{responseType:'text'})
  }
 


}
