import { Component, OnInit } from '@angular/core';
import { UserserviceService } from '../userservice.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {
  storeMsg:string="";
  constructor(public pser:UserserviceService) { }

  ngOnInit(): void {
  }
  storeUser(userregisterRef:NgForm){
    //console.log(productRef.value);  
    this.pser.storeUserDetails(userregisterRef.value).
    subscribe(res=>this.storeMsg=res,error=>console.log(error)); 

}

}
