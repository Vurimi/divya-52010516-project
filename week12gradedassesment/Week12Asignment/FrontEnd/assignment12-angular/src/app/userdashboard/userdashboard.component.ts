import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Books } from '../books';
import { UserserviceService } from '../userservice.service';

@Component({
  selector: 'app-userdashboard',
  templateUrl: './userdashboard.component.html',
  styleUrls: ['./userdashboard.component.css']
})
export class UserdashboardComponent implements OnInit {
  userName:string="";
  books:Array<Books>=[];
  constructor(public router:Router,public pser:UserserviceService) { }

  ngOnInit(): void {
    let obj = sessionStorage.getItem("name");
    if(obj!=null){
      this.userName=obj;
  }
  }
  logout() {
    sessionStorage.removeItem("name");
    this.router.navigate(["userlogin"]);
  }
  loadBooks(): void{
    //console.log("Event fired")
    this.pser.loadBookDetails().subscribe(res=>this.books=res);
  }
}
