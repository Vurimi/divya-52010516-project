import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminregisterComponent } from './adminregister/adminregister.component';
import { UsersComponent } from './users/users.component';
import { BooksComponent } from './books/books.component';
import { UserloginComponent } from './userlogin/userlogin.component';
import { UserRegisterComponent } from './user-register/user-register.component';
import { UserdashboardComponent } from './userdashboard/userdashboard.component';
import { DisplaybookComponent } from './displaybook/displaybook.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    AdminregisterComponent,
    UsersComponent,
    BooksComponent,
    UserloginComponent,
    UserRegisterComponent,
    UserdashboardComponent,
    DisplaybookComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, HttpClientModule,FormsModule,ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
