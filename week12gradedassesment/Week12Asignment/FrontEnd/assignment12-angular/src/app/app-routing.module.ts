import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminregisterComponent } from './adminregister/adminregister.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DisplaybookComponent } from './displaybook/displaybook.component';
import { LoginComponent } from './login/login.component';
import { UserRegisterComponent } from './user-register/user-register.component';
import { UserdashboardComponent } from './userdashboard/userdashboard.component';
import { UserloginComponent } from './userlogin/userlogin.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  {path:"login",component:LoginComponent},
  {path:"home",component:DashboardComponent},
  {path:"adminregister",component:AdminregisterComponent},
  {path:"CURD operation  on user",component:UsersComponent},
  {path:"userlogin",component:UserloginComponent},
  {path:"userRegister",component:UserRegisterComponent},
  {path:"userHome",component:UserdashboardComponent},
  {path:"DisplayBooks",component:DisplaybookComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
