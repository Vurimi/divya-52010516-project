export class Books {
    constructor(
        public id:number,
        public name:string,
        public generes:string,
        public url:string
    ){}
}
