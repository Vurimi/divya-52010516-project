import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { MyserviceService } from '../myservice.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginRef = new FormGroup({
    adminname:new FormControl(),
    password:new FormControl()
  });
  msg:string="";
  
  constructor(public router:Router,private MyService: MyserviceService) { }

  ngOnInit(): void {
  }
  checkAdmin() {
    let login = this.loginRef.value;
    // we write this code inside a subscribe method 
    if(login.adminname=="Admin" && login.password=="123"){
      console.log("success");
    sessionStorage.setItem("adminname",login.adminname);
        this.router.navigate(["home"]);
    }else {
        this.msg = "Invalid username or password"
        console.log("failure");
    }
    this.loginRef.reset();
  /* // this.MyService.checkByCredential(login)
      .subscribe(adminname => {
           if(adminname)
              this.router.navigate(['home']);
           else
              this.router.navigate(['login']); 
           }   );
  }*/
  }
}
