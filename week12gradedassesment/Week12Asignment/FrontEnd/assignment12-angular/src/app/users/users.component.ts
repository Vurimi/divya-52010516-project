import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';
import { Users } from '../users';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users:Array<Users>=[];
  storeMsg:string=""
  deleteMsg:string="";
  updateMsg:string="";
  flag:boolean = false;
  uid:number=0;
  username:string="";

  constructor(public pser:MyserviceService) { }

  ngOnInit(): void {
  }
  loadUsers(): void{
    //console.log("Event fired")
    this.pser.loadUserDetails().subscribe(res=>this.users=res);
  }

  storeUser(userRef:NgForm){
    //console.log(productRef.value);  
    this.pser.storeUserDetails(userRef.value).
    subscribe(res=>this.storeMsg=res,error=>console.log(error),()=>this.loadUsers());  
  }

  deleteUser(uid:number){
    //console.log(pid);
    this.pser.deleteUserDetails(uid).
    subscribe(res=>this.deleteMsg=res,error=>console.log(error),()=>this.loadUsers())
  }

  updateUser(user:Users){
    //console.log(product);
    this.flag=true;
    this.uid=user.uid;
    this.username=user.username;
  }

  updateUserName() {
    let user ={"uid":this.uid,"username":this.username}
    //console.log(product);
    this.pser.updateUserInfo(user).subscribe(result=>this.updateMsg=result,
    error=>console.log(error),
    ()=>{
    this.loadUsers();
    this.flag=false;  
    })
  }


}
